#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>

#include <sys/time.h>

#define SIZE (int) 1e8
#define NUMBER_OF_THREADS 4
int tab[SIZE];

int min;
int max;

typedef struct my_pthread_data {
  int index;
} pthread_data;

pthread_mutex_t mutex_max, mutex_min;


/**
 * Fonction exécutée par le thread
 */ 
void * my_thread_code(void * arg) {
  // Typage de la structure passée en paramètre
  pthread_data * data = (pthread_data *) arg;

  // Initialisation de la variable du code retour
  int * return_code = malloc(sizeof(int));
  if (return_code != NULL) {
    *return_code = 0;
  }
  // Le code métier consiste à trouver le min et max
  for (int i = (SIZE / NUMBER_OF_THREADS) * data->index; i < (SIZE / NUMBER_OF_THREADS) * (data->index + 1); i++)
  {
    if(tab[i] > max){
      pthread_mutex_lock(&mutex_max);
      max = tab[i];
      pthread_mutex_unlock(&mutex_max);
    }
    if(tab[i] < min){
      pthread_mutex_lock(&mutex_min);
      min = tab[i];
      pthread_mutex_unlock(&mutex_min);
    }
  }

  // Code retour du thread
  pthread_exit((void*)return_code);  
}


int main(int argc, char** argv) {
  // Variables
  pthread_t thread_ids[NUMBER_OF_THREADS]; 
  pthread_data my_data;
  int * my_return_code;

  // Initialisation du tableau
  srand(time(NULL));

  for (int i = 0; i < SIZE; i++)
  {
    tab[i] = rand();
  }

  max = tab[0]; min = tab[0];

  // Initialisation mutex
  pthread_mutex_init(&mutex_max, NULL);
  pthread_mutex_init(&mutex_min, NULL);

  // Lancement chrono
  struct timeval startTime, endTime;
  gettimeofday(&startTime, NULL);

  // Initialisation du thread :
  // Les options de création du thread sont celles par défaut d'ou l'utilisation du la valeur NULL.
  // On positionne le pointeur sur fonction ou "callback" de la fonction my_thread_code.
  // On position le pointeur de la structure que l'on a initialisé.
  for(int i = 0; i < NUMBER_OF_THREADS; i++){
    // Initialisation de la structure de données
    my_data.index = i;
    // Création du thread
    int ret =  pthread_create(&thread_ids[i], NULL, my_thread_code, &my_data);
    if (ret != 0) {
      perror("Impossible de créer le thread");
    }
  }

  // Attente de terminaison des threads
  for(int i = 0; i < NUMBER_OF_THREADS; i++){
    pthread_join(thread_ids[i], (void**)&my_return_code);
    // Affichage du code retour du thread
    printf("Code retour du thread %d : %d\n", i, *my_return_code);
  }

  // Arrêt du chrono
  gettimeofday(&endTime, NULL);

  free(my_return_code);
  pthread_mutex_destroy(&mutex_max);
  pthread_mutex_destroy(&mutex_min);

  printf("took %lu us\n", (endTime.tv_sec - startTime.tv_sec) * 1000000 + endTime.tv_usec - startTime.tv_usec);
  printf("min = %i\n", min);
  printf("max = %i\n", max);

  return 0;
}
