#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include<string.h>

#include<errno.h>


int main(int argc, char** argv)
{
	//ps eaux | grep "^root " > /dev/null && echo "root est connecté"
	// "root est connecté" dans le cas ou les fonctions précédentes renvoient 0

	int proc1, proc2;
	int status[2];

	// Création du pipe
	pipe(status);
		
	if (fork() == 0) {

		close(status[0]);
		dup2(status[1], 1);

		// Execute la commande ps eaux
		if (execlp ("ps", "ps", "eaux", (void*)0) == -1)
		{
			perror("ps eaux error");
			exit(EXIT_FAILURE);
		}

		exit(0);
	}
	
	if (fork() == 0)
	{
		close(status[1]);
		dup2(status[0], 0);

		// Création du fichier dans /dev/null
		int fd = 0;

    if ((fd = open("/dev/null", O_WRONLY)) == -1) {
        fprintf(stderr, "Failed with error %s\n", strerror(errno));
        return -1;
    }

		dup2(fd, 1);

		// Execute la commande grep root
		if (execlp ("grep", "grep", "^root", (void*)0) == -1)
		{
			perror("grep root");
			exit(EXIT_FAILURE);
		}

		exit(0);
	}

	close(status[1]);
	close(status[0]);

	// fin des processus
	wait(&proc1);
	wait(&proc2);

	// Vérifie si les processus se sont terminés normalement
	if(proc1 == 0 && proc2 == 0 ){
		write(1, "root est connecté\n", 19);
		return 0;
	}
	
	return 1; 
}