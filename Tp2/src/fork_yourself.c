#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>


int main(int argc, char** argv) {

  int pidFils = -1;

  // Création du FILS N°1 via fork()
  if ((pidFils = fork()) == 0) {
    
    // Affichage du PID du processus
    printf("[FILS] pid : %d\n", getpid());
    // Terminaison du nominal processus via code retour = 0;
    printf("[FILS] ppid : %d\n", getppid()); 

    int lastpid = getpid() % 10;

    exit(lastpid);

  }else{


    printf("[Pere] pid fils : %d\n", pidFils);

    int pidFilsmort = 0;

    wait(&pidFilsmort);

    printf("[Pere] code retour fils : %d\n", pidFilsmort/256);

  }
  
  return 0;
}