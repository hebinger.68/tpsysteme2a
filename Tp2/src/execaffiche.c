#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>


#include<errno.h>


int main(int argc, char** argv) {

  if(argc > 1){
    printf("%s\n", argv[1]);
  }
  
  if(fork() == 0){

    printf("[Fils] pid : %d\n",getpid());

    close(1);//close(2);

    char filename[] = "/tmp/proc-exerciceXXXXXX";
    int fd;
    if ((fd = mkstemp(filename)) == -1) {
        fprintf(stderr, "Failed with error %s\n", strerror(errno));
        return -1;
    }

    printf("dup : %d\n", fd);

    execl(argv[1], (char*)NULL);

    exit(0);

  }else{

    printf("[Pere] pid : %d\n",getpid());

    wait(NULL);

    printf("Un message quelconque\n");

  }
  
  return 0;
}