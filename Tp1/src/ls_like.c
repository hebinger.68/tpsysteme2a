/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<getopt.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>

#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>


#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hvi:o:";




/**
 * Binary variables
 * (could be defined in a structure)
 */
short int is_verbose_mode = 0;
char* bin_input_param = NULL;


/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv)
{

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();         
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
 
        exit(EXIT_SUCCESS);

      break;
      
      default :
        break;
    }
  } 

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if (bin_input_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }


  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %d\n", 
          "input",   bin_input_param,
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point

  /* LOREM IPSUM DOT SIR AMET */

  struct stat s;
	if(stat(bin_input_param, &s) == -1) {
		if(ENOENT == errno) {
			dprintf(STDERR, "Dir not found\n");
			perror("stat");
			exit(EXIT_FAILURE);
		} 
		else {
			perror("stat");
			exit(1);
		}
	} 
	else {

		if(S_ISDIR(s.st_mode)) {

			DIR *openeddirectory = opendir(bin_input_param);
      if (openeddirectory == NULL)
      {
        dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

        // Freeing allocated data
        free_if_needed(bin_input_param);
        // Exiting with a failure ERROR CODE (== 1)
        exit(EXIT_FAILURE);
      }

			struct dirent *readdirectory ;

      printf("%-30s %-15s %-15s %-15s %-15s %-15s\n", "Nom", "Permissions", "Proprietaire", "Groupe", "Taille", "Date de derniere maj");
			while(readdirectory = readdir(openeddirectory))
			{
				struct passwd *pwuid = getpwuid(s.st_uid);
        struct group *grgid = getgrgid(s.st_gid);
        struct tm *filetime = localtime((const time_t *)&s.st_mtime);

				stat((readdirectory->d_name), &s);
				printf("%-30s",(readdirectory->d_name));
				printf(" ");         
				printf( (S_ISDIR(s.st_mode)) ? "d" : "-");
				printf( (s.st_mode & S_IRUSR) ? "r" : "-");
				printf( (s.st_mode & S_IWUSR) ? "w" : "-");
				printf( (s.st_mode & S_IXUSR) ? "x" : "-");
				printf( (s.st_mode & S_IRGRP) ? "r" : "-");
				printf( (s.st_mode & S_IWGRP) ? "w" : "-");
				printf( (s.st_mode & S_IXGRP) ? "x" : "-");
				printf( (s.st_mode & S_IROTH) ? "r" : "-");
				printf( (s.st_mode & S_IWOTH) ? "w" : "-");
				printf( (s.st_mode & S_IXOTH) ? "x" : "-");
				printf("      ");
				printf("%-7s",pwuid->pw_name);
        printf(":");
        printf("%-7s",grgid->gr_name);
				printf(" ");
				printf("%-7d",s.st_uid);
				printf(":");
				printf("%-7d",s.st_gid);
				printf(" ");
        printf("%-15ld",s.st_size);
				printf(" ");
        printf("%d",filetime->tm_mday);
        printf("%d",filetime->tm_mon);
        printf("%d",filetime->tm_year);
        printf(" @ ");
        printf("%d",filetime->tm_hour);
        printf("h");
        printf("%d",filetime->tm_min);
        printf("\n");
			}
		} 
		else {
			dprintf(STDERR, "Not a directory\n");
			perror("stat");
			exit(EXIT_FAILURE);
		}
	}

  // Freeing allocated data
  free_if_needed(bin_input_param);


  return EXIT_SUCCESS;
}


