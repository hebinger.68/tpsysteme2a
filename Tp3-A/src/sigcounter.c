#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>

#include <sys/time.h>

#include <signal.h>

#include<sys/types.h>
#include<sys/wait.h>
#include <unistd.h>

#include<errno.h>


int count = 0;

void sig_count(int sig){
  //printf("signal recu %d\n", sig);
  count ++;
  printf(" %d\n", count);
}

void sig_term(int sig){
  printf("Fini !\n");
  exit(0);
}


int main(int argc, char** argv) {

  printf("pid :%d", getpid());
 
  signal(SIGINT, sig_count);
  signal(SIGTERM, sig_term);

  while (1);
  
  return 0;
}
