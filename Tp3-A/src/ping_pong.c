#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>

#include <sys/time.h>

#include <signal.h>

#include<sys/types.h>
#include<sys/wait.h>

#include<errno.h>

#include<unistd.h>

#include <sys/mman.h>


int pidfils;
static int *cpt_fils;
static int *cpt_pere;

void sig_hand_fils(int sig){
  printf("[fils] signal recu %d\n", sig);
  sleep(1);
  if(*cpt_fils == 13){
    kill(getppid(), SIGTERM);
    printf("[fils] j'ai gagné\n");
    return;
  }
  if (rand()%100 <= 30){
    *cpt_pere = *cpt_pere + 1;
    printf("[fils] laisse tomber - score : fils:%d pere:%d\n", *cpt_fils, *cpt_pere);
  }
  else{
    printf("[fils] pong\n");
  }
  kill(getppid(), SIGUSR2);
}

void sig_hand_pere(int sig){
  printf("[pere] signal recu %d\n", sig);
  sleep(1);
  if(*cpt_pere == 13){
    kill(pidfils, SIGTERM);
    printf("[pere] j'ai gagné\n");
    return;
  }
  if(rand()%100 >= 70){
    *cpt_fils = *cpt_fils + 1;
    printf("[pere] laisse tomber - score : fils:%d pere:%d\n", *cpt_fils, *cpt_pere);
  }
  else{
    printf("[fils] ping\n");
  }
  kill(pidfils, SIGUSR1);
}

int main(int argc, char** argv) {
  srand(time(NULL));
  cpt_fils = mmap(NULL, sizeof *cpt_fils, PROT_READ | PROT_WRITE,MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  *cpt_fils = 0;
  cpt_pere = mmap(NULL, sizeof *cpt_pere, PROT_READ | PROT_WRITE,MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  *cpt_pere = 0;

  if((pidfils = fork()) == 0){
    signal(SIGUSR1, sig_hand_fils);
    printf("fils init\n");

  }else{
    signal(SIGUSR2, sig_hand_pere);
    sleep(1);
    printf("pere init\n");
    kill(pidfils, SIGUSR1);
  
  }

  while(1);
  
  return 0;
}
